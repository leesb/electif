﻿namespace Service.Models
{
    public class RegisterModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int? UserId { get; set; }
        public int GroupId { get; set; }
    }
}
