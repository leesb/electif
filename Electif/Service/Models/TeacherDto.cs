﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class TeacherDto : UserDto
    {
        public List<ElectiveDto> Electives { get; set; }
    }
}
