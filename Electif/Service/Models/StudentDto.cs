﻿using System.Collections.Generic;

namespace Service.Models
{
    public class StudentDto : UserDto
    {
        public int? UserId {get; set;}
        public GroupDto Group { get; set; }
        public List<ElectiveDto> Electives { get; set; }
        public List<ElectiveDto> PendingElectives { get; set; }
    }
}
