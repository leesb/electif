﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class GroupDto : ErrorDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<StudentDto> Students { get; set; }
        public List<ElectiveDto> Electives { get; set; }
    }
}
