﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class ElectiveDto : ErrorDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Acronym { get; set; }
        public string Description { get; set; }
        public TeacherDto Teacher { get; set; }
        public List<GroupDto> Groups { get; set; }
        public List<StudentDto> Students { get; set; }
    }
}
