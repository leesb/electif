﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    public interface IDefaultService<TDto>
        where TDto : ErrorDto
    {
        Task<TDto> GetById(int id);
        Task<List<TDto>> Get();
        Task<TDto> Add(TDto dto);
        Task<TDto> Update(TDto dto);
        Task<TDto> Delete(int id);
    }
}
