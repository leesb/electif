﻿using Service.Models;

namespace Service.Interfaces
{
    public interface ITeacherService : IDefaultService<TeacherDto>
    {
    }
}
