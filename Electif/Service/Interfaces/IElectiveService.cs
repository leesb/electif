﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    public interface IElectiveService : IDefaultService<ElectiveDto>
    {
        Task<List<ElectiveDto>> GetByGroup(int groupId);
        Task<List<ElectiveDto>> GetByTeacher(int teacherId);

        Task<ElectiveDto> RemoveStudent(int electiveId, int studentId);
        Task<ElectiveDto> ApproveStudent(int electiveId, int studentId);
        Task<ElectiveDto> AddGroup(int electiveId, int groupId);
        Task<ElectiveDto> RemoveGroup(int electiveId, int groupId);
    }
}
