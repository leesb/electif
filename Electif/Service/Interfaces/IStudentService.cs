﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    public interface IStudentService : IDefaultService<StudentDto>
    {
        Task<StudentDto> Subscribe(int id, int electiveId);
        Task<List<StudentDto>> GetByElective(int electiveId);
    }
}
