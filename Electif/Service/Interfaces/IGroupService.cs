﻿using Service.Models;

namespace Service.Interfaces
{
    public interface IGroupService : IDefaultService<GroupDto>
    {
    }
}
