﻿using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    public interface IUserService
    {
        Task<JsonWebToken> Login(LoginModel model);
        Task<UserDto> GetById(int id);
        Task<string> GetRole(int id);
    }
}
