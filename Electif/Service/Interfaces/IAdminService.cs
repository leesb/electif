﻿using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    public interface IAdminService : IDefaultService<AdminDto>
    {
        Task<bool> ValidateStudentElective(int studentId, int electiveId);
    }
}
