﻿using System;
using System.Collections.Generic;
using System.Text;
using Backend.Models;
using Backend.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Backend.Tests.Helpers
{
    public static class InMemoryDatabaseHelper
    {
            public static T Create<T>(string guid)
                where T : DbContext
            {
                var option = new DbContextOptionsBuilder<T>()
                            .UseInMemoryDatabase(guid)
                            .Options;
                var context = Activator.CreateInstance(typeof(T), option) as T;
                return context;
            }

            public static ElectifContext addElective(this ElectifContext context, Elective elective)
            {
                context.Add(elective);
                return context;
            }

            public static ElectifContext addTeacher(this ElectifContext context, Teacher teacher)
            {
                context.Add(teacher);
                return context;
            }

            public static ElectifContext addStudent(this ElectifContext context, Student student)
            {
                context.Add(student);
                return context;
            }

            public static ElectifContext addAdmin(this ElectifContext context, Admin admin)
            {
                context.Add(admin);
                return context;
            }

            public static ElectifContext addGroup(this ElectifContext context,  Group group)
            {
                context.Add(group);
                return context;
            }
    }
}
