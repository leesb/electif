﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Backend.Controllers;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using Service.Interfaces;
using Service.Models;

namespace Backend.Tests.Controllers
{
    [TestFixture]
    public class AdminTests
    {
        private static AdminDto admin1 = new AdminDto
                                             {
                                                 Id = 2,
                                                 Email = "john@epita.fr",
                                                 Firstname = "john",
                                                 Lastname = "doe"
                                             };
        private static AdminController CreateController(IAdminService service)
        {
            var controller = new AdminController(service);
            return controller;
        }

        [Test]
        public async Task AdminGetId()
        {
            var repository = Substitute.For<IAdminService>();
            repository.GetById(10).Returns(admin1);
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task AdminGetIdNotFound()
        {
            var repository = Substitute.For<IAdminService>();
            repository.GetById(10).Returns(new AdminDto { ErrorMessage = "Not found" });
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task AdminGetList()
        {
            var repository = Substitute.For<IAdminService>();
            repository.Get().Returns(new List<AdminDto> { admin1 });
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<AdminDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task AdminGetEmptyList()
        {
            var repository = Substitute.For<IAdminService>();
            repository.Get().Returns(null as List<AdminDto>);
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<AdminDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task AdminAdd()
        {
            var repository = Substitute.For<IAdminService>();
            repository.Add(admin1).Returns(admin1);
            var controller = CreateController(repository);
            var result = await controller.Post(admin1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task AdminAddNullGetNotFound()
        {
            var repository = Substitute.For<IAdminService>();
            repository.Add(null).Returns(null as AdminDto);
            var controller = CreateController(repository);
            var result = await controller.Post(admin1);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task AdminDelete()
        {
            var repository = Substitute.For<Service.Interfaces.IAdminService>();
            repository.Delete(10).Returns(admin1);
            var controller = CreateController(repository);
            var result = await controller.Delete(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task AdminDeleteNotFound()
        {
            var repository = Substitute.For<Service.Interfaces.IAdminService>();
            repository.Delete(12).Returns(new AdminDto
            {
                ErrorMessage = $"Can't delete #12"
            });
            var controller = CreateController(repository);
            var result = await controller.Delete(12);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }
    }
}
