﻿using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Backend.Controllers;
using Service.Interfaces;
using Service.Models;

namespace Backend.Tests.Controllers
{
    [TestFixture]
    public class GroupTests
    {

        private static GroupDto group1 = new GroupDto
        {
            Id = 10,
            Name = "A1",
            Students = null,
            Electives = null
        };

        private static GroupController CreateController(IGroupService service)
        {
            var controller = new GroupController(service);
            return controller;
        }

        [Test]
        public async Task GroupGetId()
        {
            var repository = Substitute.For<IGroupService>();
            repository.GetById(10).Returns(group1);
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task GroupGetIdNotFound()
        {
            var repository = Substitute.For<IGroupService>();
            repository.GetById(10).Returns(new GroupDto { ErrorMessage = "Not found" });
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task GroupGetList()
        {
            var repository = Substitute.For<IGroupService>();
            repository.Get().Returns(new List<GroupDto> { group1 });
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<GroupDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task GroupGetEmptyList()
        {
            var repository = Substitute.For<IGroupService>();
            repository.Get().Returns(null as List<GroupDto>);
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<GroupDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task GroupAdd()
        {
            var repository = Substitute.For<IGroupService>();
            repository.Add(group1).Returns(group1);
            var controller = CreateController(repository);
            var result = await controller.Post(group1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task GroupAddNullGetNotFound()
        {
            var repository = Substitute.For<IGroupService>();
            repository.Add(null).Returns(null as GroupDto);
            var controller = CreateController(repository);
            var result = await controller.Post(group1);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task GroupDelete()
        {
            var repository = Substitute.For<Service.Interfaces.IGroupService>();
            repository.Delete(10).Returns(group1);
            var controller = CreateController(repository);
            var result = await controller.Delete(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task GroupDeleteNotFound()
        {
            var repository = Substitute.For<Service.Interfaces.IGroupService>();
            repository.Delete(12).Returns(new GroupDto
            {
                ErrorMessage = $"Can't delete #12"
            });
            var controller = CreateController(repository);
            var result = await controller.Delete(12);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }
    }
}
