using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Backend.Controllers;
using Service.Interfaces;
using Service.Models;

namespace Backend.Tests.Controllers
{
    [TestFixture]
    public class StudentTests
    {

        private static StudentDto student1 = new StudentDto
        {
            Email = "testc@gmail",
            Id = 10,
            Firstname = "Sylvain",
            Lastname = "Lebleu",
            Salt = null,
            Group = null,
            UserId = 5,
            PendingElectives = null,
            Password = "test",
            Electives = null
        };

        private static StudentController CreateController(IStudentService service)
        {
            var controller = new StudentController(service);
            return controller;
        }

        [Test]
        public async Task StudentGetId()
        {
            var repository = Substitute.For<IStudentService>();
            repository.GetById(10).Returns(student1);
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task StudentGetIdNotFound()
        {
            var repository = Substitute.For<IStudentService>();
            repository.GetById(10).Returns(new StudentDto { ErrorMessage = "Not found" });
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task StudentGetList()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Get().Returns(new List<StudentDto> { student1 });
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<StudentDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task StudentGetEmptyList()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Get().Returns(null as List<StudentDto>);
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<StudentDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task StudentAdd()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Add(student1).Returns(student1);
            var controller = CreateController(repository);
            var result = await controller.Post(student1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task StudentAddNullGetNotFound()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Add(null).Returns(null as StudentDto);
            var controller = CreateController(repository);
            var result = await controller.Post(student1);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task StudentDelete()
        {
            var repository = Substitute.For<Service.Interfaces.IStudentService>();
            repository.Delete(10).Returns(student1);
            var controller = CreateController(repository);
            var result = await controller.Delete(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task StudentDeleteNotFound()
        {
            var repository = Substitute.For<Service.Interfaces.IStudentService>();
            repository.Delete(12).Returns(new StudentDto
            {
                ErrorMessage = $"Can't delete #12"
            });
            var controller = CreateController(repository);
            var result = await controller.Delete(12);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveGetByElective()
        {
            var repository = Substitute.For<IStudentService>();
            repository.GetByElective(1).Returns(new List<StudentDto> { student1 });
            var controller = CreateController(repository);
            var result = await controller.GetByElective(1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<StudentDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task ElectiveGetByGroupEmpty()
        {
            var repository = Substitute.For<IStudentService>();
            repository.GetByElective(2).Returns(null as List<StudentDto>);
            var controller = CreateController(repository);
            var result = await controller.GetByElective(2);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<StudentDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task Subscribe()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Subscribe(2, 2).Returns(student1);
            var controller = CreateController(repository);
            var result = await controller.Subscribe(2, 2);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task SubscribeNonExistingElective()
        {
            var repository = Substitute.For<IStudentService>();
            repository.Subscribe(2, 2).Returns(new StudentDto
                                               {
                                                   ErrorMessage = "Elective not found"
                                               });
            var controller = CreateController(repository);
            var result = await controller.Subscribe(2, 2);
            Assert.AreEqual(typeof(BadRequestObjectResult), result.GetType());
        }
    }
}