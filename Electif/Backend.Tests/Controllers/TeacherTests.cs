﻿using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Backend.Controllers;
using Service.Interfaces;
using Service.Models;

namespace Backend.Tests.Controllers
{
    [TestFixture]
    public class TeacherTests
    {
        private static Service.Models.TeacherDto teacher1 = new Service.Models.TeacherDto
        {
            Id = 10,
            Email = "teacher@mail.fr",
            Electives = null,
            Firstname = "Sylvie",
            Lastname = "Leblanc",
            Password = "test",
            Salt = "hash"
        };

        private static TeacherController CreateController(ITeacherService service)
        {
            var controller = new TeacherController(service);
            return controller;
        }

        [Test]
        public async Task TeacherGetId()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.GetById(10).Returns(teacher1);
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task TeacherGetIdNotFound()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.GetById(10).Returns(new TeacherDto { ErrorMessage = "Not found" });
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task TeacherGetList()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.Get().Returns(new List<TeacherDto> { teacher1 });
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<TeacherDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task TeacherGetEmptyList()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.Get().Returns(null as List<TeacherDto>);
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<TeacherDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task TeacherAdd()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.Add(teacher1).Returns(teacher1);
            var controller = CreateController(repository);
            var result = await controller.Post(teacher1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task TeacherAddNullGetNotFound()
        {
            var repository = Substitute.For<ITeacherService>();
            repository.Add(null).Returns(null as TeacherDto);
            var controller = CreateController(repository);
            var result = await controller.Post(teacher1);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task TeacherDelete()
        {
            var repository = Substitute.For<Service.Interfaces.ITeacherService>();
            repository.Delete(10).Returns(teacher1);
            var controller = CreateController(repository);
            var result = await controller.Delete(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task TeacherDeleteNotFound()
        {
            var repository = Substitute.For<Service.Interfaces.ITeacherService>();
            repository.Delete(12).Returns(new TeacherDto
            {
                ErrorMessage = $"Can't delete #12"
            });
            var controller = CreateController(repository);
            var result = await controller.Delete(12);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }
    }
}
