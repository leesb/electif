﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Backend.Controllers;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using Service.Interfaces;
using Service.Models;

namespace Backend.Tests.Controllers
{
    [TestFixture]
    public class ElectiveTests
    {
        /*
         * DATAS
         */
        private static GroupDto group1 = new GroupDto
                                        {
                                            Id = 1,
                                            Name = "MTI"
                                        };
        private static TeacherDto teacher = new TeacherDto
                                            {
                                                Id = 1,
                                                Email = "teacher@epita.fr",
                                                Firstname = "teacher",
                                                Lastname = "rehceat"
                                            };
        private static StudentDto student1 = new StudentDto
                                             {
                                                 Id = 2,
                                                 Email = "john@epita.fr",
                                                 Firstname = "john",
                                                 Lastname = "doe",
                                                 UserId = 4242
                                             };
        private static ElectiveDto elective = new ElectiveDto
        {
            Acronym = "IANDO",
            Id = 10,
            Description = "Decouvrir Android",
            Groups = new List<GroupDto>{group1},
            Name = "Introduction à la platforme android",
            Students = null,
            Teacher = null
        };

        /*
         * Helper
         */
        private static ElectiveController CreateController(IElectiveService service)
        {
            var controller = new ElectiveController(service);
            return controller;
        }

        /*
         * Tests
         */

        [Test]
        public async Task ElectiveGetId()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetById(10).Returns(elective);
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveGetIdNotFound()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetById(10).Returns(new ElectiveDto { ErrorMessage = "Not found" });
            var controller = CreateController(repository);
            var result = await controller.Get(10);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveGetList()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.Get().Returns(new List<ElectiveDto> {elective});
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task ElectiveGetEmptyList()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.Get().Returns(null as List<ElectiveDto>);
            var controller = CreateController(repository);
            var result = await controller.Get();
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task ElectiveAdd()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.Add(elective).Returns(elective);
            var controller = CreateController(repository);
            var result = await controller.Post(elective);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveAddNullGetNotFound()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.Add(null).Returns(null as ElectiveDto);
            var controller = CreateController(repository);
            var result = await controller.Post(elective);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveDelete()
        {
            var repository = Substitute.For<Service.Interfaces.IElectiveService>();
            repository.Delete(10).Returns(elective);
            var controller = CreateController(repository);
            var result = await controller.Delete(10);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveDeleteNotFound()
        {
            var repository = Substitute.For<Service.Interfaces.IElectiveService>();
            repository.Delete(12).Returns(new ElectiveDto
                                          {
                                              ErrorMessage = $"Can't delete #12"
                                          });
            var controller = CreateController(repository);
            var result = await controller.Delete(12);
            Assert.AreEqual(typeof(NotFoundObjectResult), result.GetType());
        }

        [Test]
        public async Task ElectiveGetByGroup()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetByGroup(1).Returns(new List<ElectiveDto> { elective });
            var controller = CreateController(repository);
            var result = await controller.GetByGroup(1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task ElectiveGetByGroupEmpty()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetByGroup(2).Returns(null as List<ElectiveDto>);
            var controller = CreateController(repository);
            var result = await controller.GetByGroup(2);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(0, value.Count);
        }

        [Test]
        public async Task ElectiveGetByTeacher()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetByTeacher(1).Returns(new List<ElectiveDto> { elective });
            var controller = CreateController(repository);
            var result = await controller.GetByTeacher(1);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(1, value.Count);
        }

        [Test]
        public async Task ElectiveGetByTeacherEmpty()
        {
            var repository = Substitute.For<IElectiveService>();
            repository.GetByTeacher(2).Returns(null as List<ElectiveDto>);
            var controller = CreateController(repository);
            var result = await controller.GetByTeacher(2);
            Assert.AreEqual(typeof(OkObjectResult), result.GetType());
            var okObj = result as OkObjectResult;
            var value = okObj.Value as List<ElectiveDto>;
            Assert.AreEqual(0, value.Count);
        }
    }
}
