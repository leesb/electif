﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories;
using Backend.Tests.Helpers;
using NUnit.Framework;
using Service.Models;

namespace Backend.Tests.Repositories
{
    [TestFixture]
    public class GroupTests
    {
        private static Group dboGroup1 = new Group
                                         {
                                             Id = 1,
                                             Name = "ING1"
                                         };

        private static Group dboGroup2 = new Group
                                         {
                                             Id = 2,
                                             Name = "ING2"
                                         };

        private static GroupDto dtoGroup1 = new GroupDto
                                         {
                                             Id = 22,
                                             Name = "ING2"
                                         };

        private static ElectifContext CreateContext()
        {
            return InMemoryDatabaseHelper.Create<ElectifContext>(Guid.NewGuid().ToString());
        }

        private static GroupRepository CreateRepository(ElectifContext context)
        {
            var repository = new GroupRepository(context);
            return repository;
        }

        [Test]
        public async Task GivenNull_WhenCreate_ReturnError()
        {
            using (var context = CreateContext())
            {
                var repository = CreateRepository(context);
                var result = await repository.Add(null);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.ErrorMessage, Is.Not.Null.And.Not.Empty);
            }
        }

        [Test]
        public async Task GetById()
        {
            using (var context = CreateContext())
            {
                await context.addGroup(dboGroup1).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.GetById(1);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Id, Is.EqualTo(1));
                Assert.That(result.Name, Is.EqualTo("ING1"));
            }
        }

        [Test]
        public async Task GetById_NotExist()
        {
            using (var context = CreateContext())
            {
                await context.addGroup(dboGroup1).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.GetById(5);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Id, Is.EqualTo(0));
                Assert.That(result.ErrorMessage, Is.Not.Null);
            }
        }

        [Test]
        public async Task GetList()
        {
            using (var context = CreateContext())
            {
                await context.addGroup(dboGroup1).addGroup(dboGroup2).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.Get();

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Count, Is.EqualTo(2));
            }
        }

        [Test]
        public async Task GetEmptyList()
        {
            using (var context = CreateContext())
            {
                var repository = CreateRepository(context);
                var result = await repository.Get();

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Count, Is.EqualTo(0));
            }
        }

        [Test]
        public async Task AddGroup()
        {
            using (var context = CreateContext())
            {
                var repository = CreateRepository(context);
                var result = await repository.Add(dtoGroup1);

                Assert.That(result, Is.Not.Null);
                Assert.That(context.Groups.Count(), Is.EqualTo(1));
            }
        }
    }
}
