﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories;
using Backend.Tests.Helpers;
using NUnit.Framework;
using Service.Models;

namespace Backend.Tests.Repositories
{
    [TestFixture()]
    public class ElectiveTests
    {
        private static Teacher dboTeacher1 = new Teacher
                                     {
                                         Id = 1,
                                         Email = "teacher@mail.fr",
                                         Firstname = "Sylvie",
                                         Lastname = "Leblanc",
                                         Password = "test",
                                         Salt = "hash"
        };

        private static Teacher dboTeacher2 = new Teacher
                                             {
                                                 Id = 2,
                                                 Email = "teach2@mail.fr",
                                                 Firstname = "Sylvie",
                                                 Lastname = "Leblanc",
                                                 Password = "test",
                                                 Salt = "hash"
                                             };

        private static TeacherDto dtoTeacher1 = new TeacherDto
                                             {
                                                 Id = 5,
                                                 Email = "teacher@mail.fr",
                                                 Firstname = "Sylvie",
                                                 Lastname = "Leblanc",
                                                 Password = "test",
                                                 Salt = "hash"
                                             };

        private Elective dboElective1 = new Elective
                                       {
                                           Id = 1,
                                           Acronym = "IANDO",
                                           Name = "Initiation android",
                                           TeacherId = 1,
                                           Teacher = dboTeacher1
                                       };

        private Elective dboElective2 = new Elective
                                       {
                                           Id = 2,
                                           Acronym = "IANDO2",
                                           Name = "Initiation android2",
                                           TeacherId = 2,
                                           Teacher = dboTeacher2
                                       };

        private ElectiveDto dtoElective1 = new ElectiveDto
                                        {
                                            Id = 5,
                                            Acronym = "IANDO",
                                            Name = "Initiation android",
                                            Teacher = dtoTeacher1
                                        };

        private static ElectifContext CreateContext()
        {
            return InMemoryDatabaseHelper.Create<ElectifContext>(Guid.NewGuid().ToString());
        }

        private static ElectiveRepository CreateRepository(ElectifContext context)
        {
            var repository = new ElectiveRepository(context);
            return repository;
        }

        [Test]
        public async Task GivenNull_WhenCreate_ReturnError()
        {
            using (var context = CreateContext())
            {
                var repository = CreateRepository(context);
                var result = await repository.Add(null);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.ErrorMessage, Is.Not.Null.And.Not.Empty);
            }
        }

        [Test]
        public async Task GetById()
        {
            using (var context = CreateContext())
            {
                await context.addTeacher(dboTeacher1).addElective(dboElective1).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.GetById(1);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Id, Is.EqualTo(1));
                Assert.That(result.Acronym, Is.EqualTo("IANDO"));
            }
        }

        [Test]
        public async Task GetById_NotExist()
        {
            using (var context = CreateContext())
            {
                await context.addTeacher(dboTeacher1).addElective(dboElective1).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.GetById(5);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Id, Is.EqualTo(0));
                Assert.That(result.ErrorMessage, Is.Not.Null);
            }
        }

        [Test]
        public async Task GetList()
        {
            using (var context = CreateContext())
            {
                await context.addTeacher(dboTeacher1).addTeacher(dboTeacher2).addElective(dboElective1).addElective(dboElective2).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.Get();

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Count, Is.EqualTo(2));
            }
        }

        [Test]
        public async Task GetEmptyList()
        {
            using (var context = CreateContext())
            {
                var repository = CreateRepository(context);
                var result = await repository.Get();

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Count, Is.EqualTo(0));
            }
        }

        [Test]
        public async Task AddElective()
        {
            using (var context = CreateContext())
            {
                await context.addTeacher(dboTeacher1).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.Add(dtoElective1);

                Assert.That(result, Is.Not.Null);
                Assert.That(context.Electives.Count(), Is.EqualTo(1));
            }
        }

                [Test]
        public async Task GetListByTeacher()
        {
            using (var context = CreateContext())
            {
                await context.addTeacher(dboTeacher1).addTeacher(dboTeacher2).addElective(dboElective1).addElective(dboElective2).SaveChangesAsync();
                var repository = CreateRepository(context);
                var result = await repository.GetByTeacher(1);

                Assert.That(result, Is.Not.Null);
                Assert.That(result.Count, Is.EqualTo(1));
            }
        }
    }
}
