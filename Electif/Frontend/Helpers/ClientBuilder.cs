﻿using System;
using System.Net.Http;
using Frontend.Services;

namespace Frontend.Helpers
{
    public class ClientBuilder : IClientBuilder
    {
        public Uri BaseUri { get; set; }

        public ClientBuilder(AppSettings settings)
        {
            BaseUri = new Uri(settings.Endpoint);
        }

        private HttpClient CreateClient()
        {
            return new HttpClient {BaseAddress = BaseUri};
        }

        public AdminClient GetAdminClient(string token = null)
        {
            return new AdminClient(CreateClient(), token);
        }

        public ElectiveClient GetElectiveClient(string token = null)
        {
            return new ElectiveClient(CreateClient(), token);
        }

        public GroupClient GetGroupClient(string token = null)
        {
            return new GroupClient(CreateClient(), token);
        }

        public TeacherClient GetTeacherClient(string token = null)
        {
            return new TeacherClient(CreateClient(), token);
        }

        public StudentClient GetStudentClient(string token = null)
        {
            return new StudentClient(CreateClient(), token);
        }

        public UserClient GetUserClient()
        {
            return new UserClient(CreateClient());
        }
    }
}
