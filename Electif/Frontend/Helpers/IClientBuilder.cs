﻿using Frontend.Services;

namespace Frontend.Helpers
{
    public interface IClientBuilder
    {
        AdminClient GetAdminClient(string token = null);
        ElectiveClient GetElectiveClient(string token = null);
        GroupClient GetGroupClient(string token = null);
        TeacherClient GetTeacherClient(string token = null);
        StudentClient GetStudentClient(string token = null);
        UserClient GetUserClient();
    }
}
