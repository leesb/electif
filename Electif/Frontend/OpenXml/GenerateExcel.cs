﻿using System;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Frontend.OpenXml
{
    public static class GenerateExcelClient
    {
        public static string GenerateDocument(Service.Models.ElectiveDto elective)
        {
            string path = elective.Name + ".xlsx";
            var students = elective.Students;
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook);
            var workbookPart = spreadsheetDocument.AddWorkbookPart();
            workbookPart.Workbook = new Workbook();

            var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());

            var sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(new Sheets());
            sheets.AppendChild(new Sheet
                               {
                                   Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                                   SheetId = 42,
                                   Name = elective.Name
                               });

            worksheetPart.InsertCellInWorksheet("A", 1, "Firstname");
            worksheetPart.InsertCellInWorksheet("B", 1, "Lastname");
            worksheetPart.InsertCellInWorksheet("C", 1, "Email");
            worksheetPart.InsertCellInWorksheet("D", 1, "Group");
            worksheetPart.InsertCellInWorksheet("E", 1, "UID");
            uint index = 2;

            if (students != null && students.Count != 0)
            {
                foreach (var student in students)
                {
                    worksheetPart.InsertCellInWorksheet("A", index, student.Firstname);
                    worksheetPart.InsertCellInWorksheet("B", index, student.Lastname);
                    worksheetPart.InsertCellInWorksheet("C", index, student.Email);
                    worksheetPart.InsertCellInWorksheet("D", index, student.Group?.Name);
                    worksheetPart.InsertCellInWorksheet("E", index, student.UserId?.ToString());
                    index++;
                }
            }
            spreadsheetDocument.Save();
            spreadsheetDocument.Close();
            return path;
        }

        private static Cell InsertCellInWorksheet(this WorksheetPart worksheetPart, string columnName, uint rowIndex, string data)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            Row row;
            if (sheetData.Elements<Row>().Count(r => r.RowIndex == rowIndex) != 0)
            {
                row = sheetData.Elements<Row>().First(r => r.RowIndex == rowIndex);
            }
            else
            {
                row = new Row { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            if (row.Elements<Cell>().Any(c => c.CellReference.Value == columnName + rowIndex))
            {
                var cell = row.Elements<Cell>().First(c => c.CellReference.Value == cellReference);
                cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
                cell.CellValue = new CellValue(data);
                return cell;
            }
            else
            {
                Cell refCell = row.Elements<Cell>()
                                  .Where(cell => cell.CellReference.Value.Length == cellReference.Length)
                                  .FirstOrDefault(cell => string.Compare(cell.CellReference.Value, cellReference, StringComparison.OrdinalIgnoreCase) > 0);

                Cell newCell = new Cell
                               {
                                   CellReference = cellReference,
                                   DataType = new EnumValue<CellValues>(CellValues.SharedString),
                                   CellValue = new CellValue(data)
                               };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
    }
}
