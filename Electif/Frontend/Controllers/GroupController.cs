﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Frontend.Helpers;
using Frontend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Models;

namespace Frontend.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GroupController : BaseController
    {
        public GroupController(IClientBuilder clientBuilder) : base(clientBuilder)
        { }

        public async Task<IActionResult> Index()
        {
            using (var groupClient = GroupClient())
            {
                List<GroupDto> groups = await groupClient.Get();
                return View(groups);
            }
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return View("Index");

            using (var groupClient = GroupClient())
            {
                GroupDto group = await groupClient.GetById(id.Value);
                if (group == null)
                    return View("Index");

                return View(group);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(GroupDto group)
        {
            if (ModelState.IsValid)
            {
                using (var groupClient = GroupClient())
                {
                    await groupClient.Update(group);
                    return RedirectToAction("Index");
                }
            }
            return View(group);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return View("Index");
            }

            using (var groupClient = GroupClient())
            {
                GroupDto group = await groupClient.GetById(id.Value);
                if (group == null)
                {
                    return View("Index"); // Handle this    
                }
                return View(group);
            }
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return View("Index");
            }

            using (var groupClient = GroupClient())
            {
                GroupDto group = await groupClient.Delete(id.Value); // Http request
                if (group == null)
                {
                    return View("Index"); // Handle this    
                }

                return RedirectToAction("Index");
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Service.Models.GroupDto group)
        {
            if (ModelState.IsValid)
            {
                using (var groupClient = GroupClient())
                {
                    await groupClient.Add(group);
                    return RedirectToAction("Index");
                }
            }
            return View(group);
        }
    }
}
