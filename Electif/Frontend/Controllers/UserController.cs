﻿using System.Linq;
using System.Threading.Tasks;
using Frontend.Helpers;
using Frontend.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Service.Models;

namespace Frontend.Controllers
{
    public class UserController : BaseController
    {
        public UserController(IClientBuilder clientBuilder) : base(clientBuilder)
        { }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            using (var adminClient = AdminClient())
            using (var studentClient = StudentClient())
            using (var teacherClient = TeacherClient())
            {
                var admins = await adminClient.Get();
                var students = await studentClient.Get();
                var teachers = await teacherClient.Get();

                var model = new UserModels
                {
                    Admins = admins,
                    Teachers = teachers,
                    Students = students,
                };

                return View(model);
            }
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            using (var userClient = UserClient())
            {
                var response = await userClient.Login(model, Response.Cookies);

                if (response != null)
                {
                    await HttpContext.SignInAsync(response);
                    return Redirect("/");
                }
                else
                {
                    ModelState.AddModelError("Password", "Invalid email or password");
                    model.Password = "";
                    return View(model);
                }
            }
        }

        public async Task<IActionResult> Profile(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            string role = null;
            using (var userClient = UserClient())
            {
                role = await userClient.GetRole(id.Value);
            }
            if (role == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (role.Equals("Student"))
            {
                using (var studentClient = StudentClient())
                {
                    StudentDto student = await studentClient.GetById(id.Value);
                    if (student == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    return View(new UserProfileModel() { User = student, Electives = student.Electives, PendingElectives = student.PendingElectives, Group = student.Group, Role = role});
                }
            }
            else if (role.Equals("Admin"))
            {
                using (var adminClient = AdminClient())
                {
                    AdminDto admin = await adminClient.GetById(id.Value);
                    if (admin == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    return View(new UserProfileModel() { User = admin, Role = role });
                }
            }
            if (role.Equals("Teacher"))
            {
                using (var teacherClient = TeacherClient())
                {
                    TeacherDto teacher = await teacherClient.GetById(id.Value);
                    if (teacher == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    return View(new UserProfileModel() { User = teacher, Electives = teacher.Electives, Role = role });
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            Response.Cookies.Delete("jwt");
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            using (var groupClient = GroupClient())
            {
                var groups = await groupClient.Get();
                var groupsList = new SelectList(groups, "Id", "Name");

                return View(new CreateUserModel() { Groups = groupsList });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create(CreateUserModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.RegisterModel.Type == "Admin")
                {
                    RegisterModel rm = model.RegisterModel;
                    using (var client = AdminClient())
                    {
                        await client.Add(new AdminDto()
                        {
                            Firstname = rm.Firstname,
                            Lastname = rm.Lastname,
                            Email = rm.Email,
                            Password = rm.Password
                        });
                        return RedirectToAction("Index");
                    }
                } else if (model.RegisterModel.Type == "Teacher")
                {
                    RegisterModel rm = model.RegisterModel;
                    using (var client = TeacherClient())
                    {
                        await client.Add(new TeacherDto()
                        {
                            Firstname = rm.Firstname,
                            Lastname = rm.Lastname,
                            Email = rm.Email,
                            Password = rm.Password
                        });
                        return RedirectToAction("Index");
                    }
                } else if (model.RegisterModel.Type == "Student")
                {
                    RegisterModel rm = model.RegisterModel;
                    using (var client = StudentClient())
                    {
                        await client.Add(new StudentDto()
                        {
                            Firstname = rm.Firstname,
                            Lastname = rm.Lastname,
                            Email = rm.Email,
                            Password = rm.Password,
                            UserId = rm.UserId,
                            Group = new GroupDto() { Id = rm.GroupId }
                        });
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(model);
        }
    }
}
