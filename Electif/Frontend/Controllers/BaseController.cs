﻿using Frontend.Helpers;
using Frontend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Frontend.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly IClientBuilder clientBuilder;

        protected BaseController(IClientBuilder clientBuilder)
        {
            this.clientBuilder = clientBuilder;
        }

        private string GetJwt()
        {
            return Request?.Cookies?.ContainsKey("jwt") == true
                       ? Request.Cookies["jwt"]
                       : null;
        }

        protected AdminClient AdminClient()
        {
            return clientBuilder.GetAdminClient(GetJwt());
        }

        protected StudentClient StudentClient()
        {
            return clientBuilder.GetStudentClient(GetJwt());
        }

        protected TeacherClient TeacherClient()
        {
            return clientBuilder.GetTeacherClient(GetJwt());
        }

        protected ElectiveClient ElectiveClient()
        {
            return clientBuilder.GetElectiveClient(GetJwt());
        }

        protected GroupClient GroupClient()
        {
            return clientBuilder.GetGroupClient(GetJwt());
        }

        protected UserClient UserClient()
        {
            return clientBuilder.GetUserClient();
        }
    }
}
