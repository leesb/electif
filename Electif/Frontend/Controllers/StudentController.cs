﻿using System.Linq;
using System.Threading.Tasks;
using Frontend.Helpers;
using Microsoft.AspNetCore.Mvc;
using Service.Models;

namespace Frontend.Controllers
{
    public class StudentController : BaseController
    {
        public StudentController(IClientBuilder clientBuilder) : base(clientBuilder)
        { }


        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Index", "Home");
        }


        public async Task<IActionResult> Approve(int? id, int? electiveId)
        {
            if (id == null || electiveId == null)
            {
                return RedirectToAction("Index", "Home");
            }
            using (var adminClient = AdminClient())
            {
                await adminClient.ValidateStudentElective(id.Value, electiveId.Value);
                return RedirectToAction("Profile", "User", new {id = id.Value});
            }
        }
    }
}
