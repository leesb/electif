using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Frontend.Helpers;
using System.Linq;
using Frontend.OpenXml;
using Microsoft.AspNetCore.Authorization;
using Service.Models;
using Frontend.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Frontend.Controllers
{
    public class ElectiveController : BaseController
    {

        public ElectiveController(IClientBuilder clientBuilder) : base(clientBuilder)
        { }

        public async Task<IActionResult> Index()
        {
            using (var client = ElectiveClient())
            {
                bool parsed = int.TryParse(User.Claims.FirstOrDefault(c => c.Type.Equals("Id"))?.Value, out int id);
                if (!parsed || User.IsInRole("Admin") || User.IsInRole("Teacher"))
                {
                    var electives = await client.Get();
                    return View(electives);
                }

                using (var studentClient = StudentClient())
                {
                    var user = await studentClient.GetById(id);
                    var electives = await client.GetByGroup(user.Group.Id);
                    return View("AvailableElectives", electives);
                }
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Create()
        {
            using (var teacherClient = TeacherClient())
            using (var groupClient = GroupClient())
            {
                var teachers = await teacherClient.Get();
                var groups = await groupClient.Get();
                var teachersList = new SelectList(teachers.Select(t => new { t.Id, Name = t.ToString() }), "Id", "Name");
                var groupsList = new SelectList(groups, "Id", "Name");
                return View(new CreateElectiveModel { Teachers = teachersList, Groups = groupsList });
            }
        }

        public async Task<IActionResult> AvailableElectives()
        {
            using (var client = ElectiveClient())
            {
                bool parsed = int.TryParse(User.Claims.FirstOrDefault(c => c.Type.Equals("Id"))?.Value, out int id);
                if (!parsed || User.IsInRole("Admin") || User.IsInRole("Teacher"))
                {
                    var electives = await client.Get();
                    return View("Index", electives);
                }

                using (var studentClient = StudentClient())
                {
                    var user = await studentClient.GetById(id);
                    var electives = await client.GetByGroup(user.Group.Id);
                    return View(electives);
                }
            }
        }

        public async Task<IActionResult> Subscribe(int? id)
        {
            bool parsed = int.TryParse(User.Claims.FirstOrDefault(c => c.Type.Equals("Id"))?.Value, out int i);
            if (!id.HasValue || !parsed)
                return RedirectToAction("AvailableElectives");

            using (var studentClient = StudentClient())
            {
                var user = await studentClient.GetById(i);
                var electives = await studentClient.Subscribe(i, id.Value);
            }
            return RedirectToAction("AvailableElectives");
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            using (var client = ElectiveClient())
            {
                ElectiveDto elective = await client.GetById(id.Value);
                if (elective == null)
                {
                    var electives = await client.Get();
                    return View("Index", electives);
                }
                return View(elective);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<ActionResult> Create(CreateElectiveModel model)
        {
            if (ModelState.IsValid)
            {
                model.Elective.Teacher = new TeacherDto {Id = model.SelectedTeacherId};
                model.Elective.Groups = new List<GroupDto> { new GroupDto {Id = model.SelectedGroupId}};
                using (var client = ElectiveClient())
                {
                    await client.Add(model.Elective);
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Download(int electiveId)
        {
            using (var electiveClient = ElectiveClient())
            {
                ElectiveDto elective = await electiveClient.GetById(electiveId);
                string path = GenerateExcelClient.GenerateDocument(elective);
                var memory = new MemoryStream();
                await using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Path.GetFileName(path));
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                RedirectToAction("Index");
            }

            using (var electiveClient = ElectiveClient())
            {
                ElectiveDto electiveDto = await electiveClient.Delete(id.Value); // Http request
                if (electiveDto == null)
                {
                    RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddGroup(int electiveId)
        {
            using (var groupClient = GroupClient())
            using (var electiveClient = ElectiveClient())
            {
                var groups = await groupClient.Get();
                var elective = await electiveClient.GetById(electiveId);
                var filtered = new List<GroupDto>();

                foreach (var group in groups)
                {
                    if (elective.Groups.FirstOrDefault(g => g.Id == group.Id) == null)
                        filtered.Add(group);
                }

                var groupList = new SelectList(filtered, "Id", "Name");

                var model = new AddGroupElectiveViewModel { Elective = elective, Groups = groupList };
                return View(model);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddGroup(AddGroupElectiveViewModel model)
        {
            using (var electiveClient = ElectiveClient())
            {
                await electiveClient.AddGroup(model.ElectiveId, model.SelectedGroupId);
                return RedirectToAction("AddGroup", new { electiveId = model.ElectiveId });
            }
        }
    }
}