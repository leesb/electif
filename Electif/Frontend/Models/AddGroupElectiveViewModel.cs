﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Service.Models;

namespace Frontend.Models
{
    public class AddGroupElectiveViewModel
    {
        public ElectiveDto Elective { get; set; }
        public SelectList Groups { get; set; }
        public int SelectedGroupId { get; set; }
        public int ElectiveId { get; set; }
    }
}
