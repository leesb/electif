﻿using Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Frontend.Models
{
    public class UserTableModel
    {
        public IEnumerable<UserDto> Users { get; set; }

        public string ControllerName { get; set; }

    }
}
