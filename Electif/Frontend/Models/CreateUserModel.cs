﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Service.Models;

namespace Frontend.Models
{
    public class CreateUserModel
    {
        public RegisterModel RegisterModel { get; set; }
        public SelectList Groups { get; set; }

    }
}
