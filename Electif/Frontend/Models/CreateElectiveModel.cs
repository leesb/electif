﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Frontend.Models
{
    public class CreateElectiveModel
    {
        public ElectiveDto Elective { get; set; }
        public SelectList Teachers { get; set; }
        public SelectList Groups { get; set; }

        public int SelectedTeacherId { get; set; }
        public int SelectedGroupId { get; set; }
    }
}
