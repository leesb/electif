﻿using Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Frontend.Models
{
    public class UserModels
    {
        public List<AdminDto> Admins { get; set; }
        public List<StudentDto> Students { get; set; }
        public List<TeacherDto> Teachers { get; set; }
    }
}
