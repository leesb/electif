﻿using Microsoft.AspNetCore.Identity;

namespace Frontend.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
