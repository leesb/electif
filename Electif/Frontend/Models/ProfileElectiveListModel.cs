﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Models;

namespace Frontend.Models
{
    public class ProfileElectiveListModel
    {
        public List<ElectiveDto> electiveList { get; set; }
        public bool isPending { get; set; }

        public int studentId { get; set; }
    }
}
