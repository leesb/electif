﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Models;

namespace Frontend.Models
{
    public class UserProfileModel
    {
        public UserDto User { get; set; }
        public string Role { get; set; }
        public List<ElectiveDto> Electives { get; set; }
        public List<ElectiveDto> PendingElectives { get; set; }
        public GroupDto Group { get; set; }
    }
}
