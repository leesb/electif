﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Service.Interfaces;
using Service.Models;

namespace Frontend.Services
{
    public class StudentClient : AClient<StudentDto>, IStudentService
    {
        public StudentClient(HttpClient client, string token) : base(client, token)
        {
        }

        public async Task<StudentDto> GetById(int id)
        {
            return await base.Get($"student/{id}");
        }

        public async Task<List<StudentDto>> Get()
        {
            return await base.GetList("student");
        }

        public async Task<StudentDto> Add(StudentDto dto)
        {
            return await base.Post("student", dto);
        }

        public async Task<StudentDto> Update(StudentDto dto)
        {
            return await base.Put($"student/{dto.Id}", dto);
        }

        public async Task<StudentDto> Delete(int id)
        {
            return await base.Delete($"student/{id}");
        }

        public async Task<StudentDto> Subscribe(int id, int electiveId)
        {
            return await base.Post($"student/{id}/subscribe/{electiveId}", null);
        }

        public async Task<List<StudentDto>> GetByElective(int electiveId)
        {
            return await base.GetList($"student/elective/{electiveId}");
        }
    }
}
