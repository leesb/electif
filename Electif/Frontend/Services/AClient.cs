﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Frontend.Services
{
    public abstract class AClient<T> : IDisposable where T : class
    {
        public HttpClient Client { get; set; }

        protected AClient(HttpClient client, string token = null)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (token != null)
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            Client = client;
        }

        protected async Task<T> Get(string endpoint)
        {
            var response = await Client.GetAsync(endpoint);
            try
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<T>();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }


        protected async Task<List<T>> GetList(string endpoint)
        {
            var response = await Client.GetAsync(endpoint);
            try
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<List<T>>();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        protected async Task<T> Delete(string endpoint)
        {
            var response = await Client.DeleteAsync(endpoint);
            try
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<T>();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        protected async Task<T> Put(string endpoint, T data)
        {
            var response = await Client.PutAsJsonAsync(endpoint, data);
            try
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<T>();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        protected async Task<T> Post(string endpoint, T data)
        {
            var response = await Client.PostAsJsonAsync(endpoint, data);
            try
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<T>();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }
}
