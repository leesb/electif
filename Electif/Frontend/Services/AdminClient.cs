﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Frontend.Services
{
    public class AdminClient : AClient<AdminDto>, IAdminService
    {
        public AdminClient(HttpClient client, string token)
            : base(client, token)
        { }

        public async Task<AdminDto> GetById(int id)
        {
            return await base.Get($"admin/{id}");
        }

        public async Task<List<AdminDto>> Get()
        {
            return await base.GetList("admin");
        }

        public async Task<AdminDto> Add(AdminDto dto)
        {
            return await base.Post("admin", dto);
        }

        public async Task<AdminDto> Update(AdminDto dto)
        {
            return await base.Put($"admin/{dto.Id}", dto);
        }

        public async Task<AdminDto> Delete(int id)
        {
            return await base.Delete($"admin/{id}");
        }

        public async Task<bool> ValidateStudentElective(int studentId, int electiveId)
        {
            await base.Post($"admin/approve/{studentId}/{electiveId}", null);
            return true;
        }
    }
}
