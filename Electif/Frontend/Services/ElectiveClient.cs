﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Service.Interfaces;
using Service.Models;

namespace Frontend.Services
{
    public class ElectiveClient : AClient<ElectiveDto>, IElectiveService
    {
        public ElectiveClient(HttpClient client, string token) : base(client, token)
        {
        }

        public async Task<ElectiveDto> GetById(int id)
        {
            return await base.Get($"elective/{id}");
        }

        public async Task<List<ElectiveDto>> Get()
        {
            return await base.GetList("elective");
        }

        public async Task<ElectiveDto> Add(ElectiveDto dto)
        {
            return await base.Post("elective", dto);
        }

        public async Task<ElectiveDto> Update(ElectiveDto dto)
        {
            return await base.Put($"elective/{dto.Id}", dto);
        }

        public async Task<ElectiveDto> Delete(int id)
        {
            return await base.Delete($"elective/{id}");
        }

        public async Task<List<ElectiveDto>> GetByGroup(int groupId)
        {
            return await base.GetList($"elective/group/{groupId}");
        }

        public async Task<List<ElectiveDto>> GetByTeacher(int teacherId)
        {
            return await base.GetList($"elective/teacher/{teacherId}");
        }

        public async Task<ElectiveDto> RemoveStudent(int electiveId, int studentId)
        {
            return await base.Delete($"elective/{electiveId}/student/{studentId}");
        }

        public async Task<ElectiveDto> ApproveStudent(int electiveId, int studentId)
        {
            return await base.Post($"elective/{electiveId}/student/approve/{studentId}", null);
        }

        public async Task<ElectiveDto> AddGroup(int electiveId, int groupId)
        {
            return await base.Post($"elective/{electiveId}/group/{groupId}", null);
        }

        public async Task<ElectiveDto> RemoveGroup(int electiveId, int groupId)
        {
            return await base.Delete($"elective/{electiveId}/group/{groupId}");
        }
    }
}
