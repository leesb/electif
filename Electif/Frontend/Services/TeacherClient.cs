﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Service.Interfaces;
using Service.Models;

namespace Frontend.Services
{
    public class TeacherClient : AClient<TeacherDto>, ITeacherService
    {
        public TeacherClient(HttpClient client, string token) : base(client, token)
        {
        }

        public async Task<TeacherDto> GetById(int id)
        {
            return await base.Get($"teacher/{id}");
        }

        public async Task<List<TeacherDto>> Get()
        {
            return await base.GetList("teacher");
        }

        public async Task<TeacherDto> Add(TeacherDto dto)
        {
            return await base.Post("teacher", dto);
        }

        public async Task<TeacherDto> Update(TeacherDto dto)
        {
            return await base.Put($"teacher/{dto.Id}", dto);
        }

        public async Task<TeacherDto> Delete(int id)
        {
            return await base.Delete($"teacher/{id}");
        }
    }
}
