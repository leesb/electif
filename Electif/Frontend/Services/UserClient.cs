﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Frontend.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Service.Models;

namespace Frontend.Services
{
    public class UserClient : AClient<UserDto>
    {
        public UserClient(HttpClient client) : base(client)
        { }


        public async Task<string> GetRole(int id)
        {
            try
            {
                var response = await Client.GetAsync($"user/{id}/role");
                response.EnsureSuccessStatusCode();
                var role = await response.Content.ReadAsAsync<string>();
                return role;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<ClaimsPrincipal> Login(LoginViewModel model, IResponseCookies cookies)
        {
            var response = await Client.PostAsJsonAsync("user/login", model);
            try
            {
                response.EnsureSuccessStatusCode();

                var token = await response.Content.ReadAsAsync<JsonWebToken>();
                if (!token.Success)
                    return null;
                cookies.Append("jwt", token.Token);
                var claims = new List<Claim>
                             {
                                 new Claim(ClaimTypes.Email, token.Email),
                                 new Claim(ClaimTypes.GivenName, token.Firstname),
                                 new Claim(ClaimTypes.Name, token.Lastname),
                                 new Claim(ClaimTypes.Role, token.Role),
                                 new Claim("Id", token.Id.ToString(), ClaimValueTypes.Integer32)
                             };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);

                return principal;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }
    }
}
