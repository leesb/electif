﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Service.Interfaces;
using Service.Models;

namespace Frontend.Services
{
    public class GroupClient : AClient<GroupDto>, IGroupService
    {
        public GroupClient(HttpClient client, string token) : base(client, token)
        {
        }
        public async Task<GroupDto> GetById(int id)
        {
            return await base.Get($"group/{id}");
        }

        public async Task<List<GroupDto>> Get()
        {
            return await base.GetList("group");
        }

        public async Task<GroupDto> Add(GroupDto dto)
        {
            return await base.Post("group", dto);
        }

        public async Task<GroupDto> Update(GroupDto dto)
        {
            return await base.Put($"group/{dto.Id}", dto);
        }

        public async Task<GroupDto> Delete(int id)
        {
            return await base.Delete($"group/{id}");
        }
    }
}
