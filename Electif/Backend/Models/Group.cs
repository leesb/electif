﻿using System.Collections.Generic;

namespace Backend.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Student> Students { get; set; }
        public List<ElectiveGroup> Electives { get; set; }
    }
}
