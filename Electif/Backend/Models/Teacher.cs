﻿using System.Collections.Generic;

namespace Backend.Models
{
    public class Teacher : User
    {
        public List<Elective> Electives { get; set; }
    }
}
