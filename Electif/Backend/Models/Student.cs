﻿using System.Collections.Generic;

namespace Backend.Models
{
    public class Student : User
    {
        public int GroupId { get; set; }
        public Group Group { get; set; }

        public List<StudentElective> Electives { get; set; }
    }
}
