﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class StudentElective
    {
        public int StudentId { get; set; }
        public int ElectiveId { get; set; }
        public bool Approved { get; set; }

        public Student Student { get; set; }
        public Elective Elective { get; set; }
    }
}
