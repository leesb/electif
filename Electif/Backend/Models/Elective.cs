﻿using System.Collections.Generic;

namespace Backend.Models
{
    public class Elective
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Acronym { get; set; }
        public string Description { get; set; }

        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public List<ElectiveGroup> Groups { get; set; }
        public List<StudentElective> Students { get; set; }
    }
}
