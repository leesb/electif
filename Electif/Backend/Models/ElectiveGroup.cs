﻿namespace Backend.Models
{
    public class ElectiveGroup
    {
        public int ElectiveId { get; set; }
        public int GroupId { get; set; }

        public Elective Elective { get; set; }
        public Group Group { get; set; }
    }
}
