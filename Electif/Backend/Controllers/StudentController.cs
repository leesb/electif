﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{
    [Route("api/student")]
    [ApiController]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService studentRepository;

        public StudentController(IStudentService studentRepository)
        {
            this.studentRepository = studentRepository;
        }

        /// <summary>
        /// Get list of students
        /// </summary>
        /// <returns>The list of students</returns>
        /// <response code="404">There is no students</response>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var studentList = await studentRepository.Get() ?? new System.Collections.Generic.List<StudentDto>();
            return Ok(studentList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var student = await studentRepository.GetById(id);
            if (student == null || !string.IsNullOrEmpty(student.ErrorMessage))
            {
                ErrorDto error = student;
                return NotFound(error);
            }
            return Ok(student);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StudentDto value)
        {
            var student = await studentRepository.Add(value);
            if (student == null || !string.IsNullOrEmpty(student.ErrorMessage))
            {
                ErrorDto error = student;
                return NotFound(error);
            }
            return Ok(student);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] StudentDto value)
        {
            var student = await studentRepository.Update(value);
            if (student == null || !string.IsNullOrEmpty(student.ErrorMessage))
            {
                ErrorDto error = student;
                return NotFound(error);
            }
            return Ok(student);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var student = await studentRepository.Delete(id);
            if (student == null || !string.IsNullOrEmpty(student.ErrorMessage))
            {
                ErrorDto error = student;
                return NotFound(error);
            }
            return Ok(student);
        }

        [HttpPost("{id}/subscribe/{electiveId}")]
        public async Task<IActionResult> Subscribe(int id, int electiveId)
        {
            var result = await studentRepository.Subscribe(id, electiveId);
            if (!string.IsNullOrEmpty(result.ErrorMessage))
                return BadRequest(result);
            return Ok(result);
        }

        [HttpGet("elective/{electiveId}")]
        public async Task<IActionResult> GetByElective(int electiveId)
        {
            var studentList = await studentRepository.GetByElective(electiveId) ?? new System.Collections.Generic.List<StudentDto>();
            return Ok(studentList);
        }
    }
}
