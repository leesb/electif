﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{
    [Route("api/teacher")]
    [ApiController]
    [Authorize]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherService teacherRepository;

        public TeacherController(ITeacherService teacherRepository)
        {
            this.teacherRepository = teacherRepository;
        }
        /// <summary>
        /// Get list of teachers
        /// </summary>
        /// <returns>The list of teachers</returns>
        /// <response code="404">There is no teachers</response>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var teacherList = await teacherRepository.Get() ?? new List<TeacherDto>();
            return Ok(teacherList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var teacher = await teacherRepository.GetById(id);
            if (teacher == null || !string.IsNullOrEmpty(teacher.ErrorMessage))
            {
                ErrorDto error = teacher;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(teacher);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TeacherDto value)
        {
            var teacher = await teacherRepository.Add(value);
            if (teacher == null || !string.IsNullOrEmpty(teacher.ErrorMessage))
            {
                ErrorDto error = teacher;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(teacher);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] TeacherDto value)
        {
            var teacher = await teacherRepository.Update(value);
            if (teacher == null || !string.IsNullOrEmpty(teacher.ErrorMessage))
            {
                ErrorDto error = teacher;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(teacher);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var teacher = await teacherRepository.Delete(id);
            if (teacher == null || !string.IsNullOrEmpty(teacher.ErrorMessage))
            {
                ErrorDto error = teacher;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(teacher);
        }
    }
}
