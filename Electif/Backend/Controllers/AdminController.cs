﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{
    [Route("api/admin")]
    [ApiController]
    [Authorize]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService adminRepository;

        public AdminController(IAdminService adminRepository)
        {
            this.adminRepository = adminRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var adminList = await adminRepository.Get() ?? new System.Collections.Generic.List<AdminDto>();
            return Ok(adminList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var admin = await adminRepository.GetById(id);
            if (admin == null || !string.IsNullOrEmpty(admin.ErrorMessage))
            {
                ErrorDto error = admin;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(admin);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AdminDto value)
        {
            var admin = await adminRepository.Add(value);
            if (admin == null || !string.IsNullOrEmpty(admin.ErrorMessage))
            {
                ErrorDto error = admin;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(admin);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] AdminDto value)
        {
            var admin = await adminRepository.Update(value);
            if (admin == null || !string.IsNullOrEmpty(admin.ErrorMessage))
            {
                ErrorDto error = admin;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(admin);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var admin = await adminRepository.Delete(id);
            if (admin == null || !string.IsNullOrEmpty(admin.ErrorMessage))
            {
                ErrorDto error = admin;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(admin);
        }

        [HttpPost("approve/{studentId}/{electiveId}")]
        public async Task<IActionResult> ValidateStudentElective(int studentId, int electiveId)
        {
            var isValidate = await adminRepository.ValidateStudentElective(studentId, electiveId);
            if (!isValidate)
            {
                return new NotFoundObjectResult(new ErrorDto {ErrorMessage = "Elective not pending"});
            }
            return Ok();
        }
    }
}
