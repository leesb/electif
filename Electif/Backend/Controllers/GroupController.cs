﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{
    [Route("api/group")]
    [ApiController]
    [Authorize]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService groupRepository;

        public GroupController(IGroupService groupRepository)
        {
            this.groupRepository = groupRepository;
        }
        /// <summary>
        /// Get list of groups
        /// </summary>
        /// <returns>The list of groups</returns>
        /// <response code="404">There is no groups</response>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var groupList = await groupRepository.Get() ?? new List<GroupDto>();
            return Ok(groupList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var group = await groupRepository.GetById(id);
            if (group == null || !string.IsNullOrEmpty(group.ErrorMessage))
            {
                ErrorDto error = group;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(group);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GroupDto value)
        {
            var group = await groupRepository.Add(value);
            if (group == null || !string.IsNullOrEmpty(group.ErrorMessage))
            {
                ErrorDto error = group;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(group);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] GroupDto value)
        {
            var group = await groupRepository.Update(value);
            if (group == null || !string.IsNullOrEmpty(group.ErrorMessage))
            {
                ErrorDto error = group;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(group);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var group = await groupRepository.Delete(id);
            if (group == null || !string.IsNullOrEmpty(group.ErrorMessage))
            {
                ErrorDto error = group;
                return new NotFoundObjectResult(error);
            }
            return new OkObjectResult(group);
        }
    }
}
