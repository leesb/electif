﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Backend.Helpers;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{

    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IAdminService adminService;
        private readonly IStudentService studentService;
        private readonly ITeacherService teacherService;
        private readonly IUserService userService;

        public UserController(IAdminService adminService,
                              IStudentService studentService,
                              ITeacherService teacherService,
                              IUserService userService)
        {
            this.adminService = adminService;
            this.studentService = studentService;
            this.teacherService = teacherService;
            this.userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var user = await userService.GetById(id);
            if (user is StudentDto student)
                return Ok(student);
            if (user is AdminDto admin)
                return Ok(admin);
            if (user is TeacherDto teacher)
                return Ok(teacher);
            return BadRequest(user);
        }

        [HttpGet("{id}/role")]
        public async Task<IActionResult> GetRole(int id)
        {
            var role = await userService.GetRole(id);
            if (role == null)
                return NotFound($"User {id} does not exist");
            return Ok(role);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var salt = SecurityHelper.GenerateSalt();
            var hashed = SecurityHelper.HashSHA1(salt, model.Password);
            var stringSalt = Convert.ToBase64String(salt);

            if (model.Type.Equals("admin", StringComparison.InvariantCultureIgnoreCase))
            {
                var admin = new AdminDto
                            {
                                Email = model.Email,
                                Password = hashed,
                                Firstname = model.Firstname,
                                Lastname = model.Lastname,
                                Salt = stringSalt
                            };
                return Ok(await adminService.Add(admin));
            }
            if (model.Type.Equals("student", StringComparison.InvariantCultureIgnoreCase))
            {
                var student = new StudentDto
                            {
                                Email = model.Email,
                                Password = hashed,
                                Firstname = model.Firstname,
                                Lastname = model.Lastname,
                                Salt = stringSalt,
                                UserId = model.UserId,
                                Group = new GroupDto
                                        {
                                            Id = model.GroupId
                                        }
                            };
                return Ok(await studentService.Add(student));
            }
            if (model.Type.Equals("teacher", StringComparison.InvariantCultureIgnoreCase))
            {
                var teacher = new TeacherDto
                              {
                                  Email = model.Email,
                                  Password = hashed,
                                  Firstname = model.Firstname,
                                  Lastname = model.Lastname,
                                  Salt = stringSalt
                              };
                return Ok(await teacherService.Add(teacher));
            }

            return BadRequest("Invalid Type");
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var token = await userService.Login(model);
            if (token == null || !token.Success)
                return BadRequest(new ErrorDto {ErrorMessage = "Invalid login or password"});
            return Ok(token);
        }
    }
}
