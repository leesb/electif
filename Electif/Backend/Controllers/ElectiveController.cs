﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Models;

namespace Backend.Controllers
{
    [Route("api/elective")]
    [ApiController]
    [Authorize]
    public class ElectiveController : ControllerBase
    {
            private readonly IElectiveService electiveRepository;

            public ElectiveController(IElectiveService electiveRepository)
            {
                this.electiveRepository = electiveRepository;
            }
            /// <summary>
            /// Get list of electives
            /// </summary>
            /// <returns>The list of electives</returns>
            /// <response code="404">There is no electives</response>
            [HttpGet]
            public async Task<IActionResult> Get()
            {
                var electiveList = await electiveRepository.Get() ?? new List<ElectiveDto>();
                return Ok(electiveList);
            }

            [HttpGet("{id}")]
            public async Task<IActionResult> Get(int id)
            {
                var elective = await electiveRepository.GetById(id);
                if (elective == null || !string.IsNullOrEmpty(elective.ErrorMessage))
                {
                    ErrorDto error = elective;
                    return new NotFoundObjectResult(error);
                }
                return new OkObjectResult(elective);
            }

            [HttpPost]
            public async Task<IActionResult> Post([FromBody] ElectiveDto value)
            {
                var elective = await electiveRepository.Add(value);
                if (elective == null || !string.IsNullOrEmpty(elective.ErrorMessage))
                {
                    ErrorDto error = elective;
                    return new NotFoundObjectResult(error);
                }
                return new OkObjectResult(elective);
            }

            [HttpPut("{id}")]
            public async Task<IActionResult> Put([FromBody] ElectiveDto value)
            {
                var elective = await electiveRepository.Update(value);
                if (elective == null || !string.IsNullOrEmpty(elective.ErrorMessage))
                {
                    ErrorDto error = elective;
                    return new NotFoundObjectResult(error);
                }
                return new OkObjectResult(elective);
            }

            [HttpDelete("{id}")]
            public async Task<IActionResult> Delete(int id)
            {
                var elective = await electiveRepository.Delete(id);
                if (elective == null || !string.IsNullOrEmpty(elective.ErrorMessage))
                {
                    ErrorDto error = elective;
                    return new NotFoundObjectResult(error);
                }
                return new OkObjectResult(elective);
            }

            [HttpGet("group/{groupId}")]
            public async Task<IActionResult> GetByGroup(int groupId)
            {
                var electiveList = await electiveRepository.GetByGroup(groupId) ?? new List<ElectiveDto>();
                return Ok(electiveList);
            }

            [HttpGet("teacher/{teacherId}")]
            public async Task<IActionResult> GetByTeacher(int teacherId)
            {
                var electiveList = await electiveRepository.GetByTeacher(teacherId) ?? new List<ElectiveDto>();
                return Ok(electiveList);
            }

            [HttpDelete("{electiveId}/student/{studentId}")]
            public async Task<IActionResult> RemoveStudent(int electiveId, int studentId)
            {
                var elective = await electiveRepository.RemoveStudent(electiveId, studentId);
                return Ok(elective);
            }

            [HttpPost("{electiveId}/student/approve/{studentId}")]
            public async Task<IActionResult> ApproveStudent(int electiveId, int studentId)
            {
                var elective = await electiveRepository.ApproveStudent(electiveId, studentId);
                return Ok(elective);
            }

            [HttpDelete("{electiveId}/group/{groupId}")]
            public async Task<IActionResult> RemoveGroup(int electiveId, int groupId)
            {
                var elective = await electiveRepository.RemoveGroup(electiveId, groupId);
                return Ok(elective);
            }

            [HttpPost("{electiveId}/group/{groupId}")]
            public async Task<IActionResult> AddGroup(int electiveId, int groupId)
            {
                var elective = await electiveRepository.AddGroup(electiveId, groupId);
                return Ok(elective);
            }
    }
}
