﻿using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class ElectiveConverter
    {
        public static ElectiveDto ToBaseDto(this Elective e)
        {
            if (e == null)
                return null;
            return new ElectiveDto
                   {
                       Id = e.Id,
                       Name = e.Name,
                       Description = e.Description,
                       Acronym = e.Acronym,
                       Teacher = e.Teacher?.ToBaseDto()
                   };
        }

        public static Elective ToBaseDbo(this ElectiveDto e)
        {
            if (e == null)
                return null;
            return new Elective
                   {
                       Id = e.Id,
                       Name = e.Name,
                       Description = e.Description,
                       Acronym = e.Acronym,
                       TeacherId = e.Teacher?.Id ?? 0
            };
        }

        public static ElectiveDto ToDto(this Elective e)
        {
            if (e == null)
                return null;
            var elective = new ElectiveDto
                           {
                               Id = e.Id,
                               Name = e.Name,
                               Description = e.Description,
                               Acronym = e.Acronym,
                               Teacher = e.Teacher?.ToBaseDto()
                           };
            List<GroupDto> groups = e.Groups?.Select(g => g.Group?.ToBaseDto()).ToList();
            List<StudentDto> students = e.Students?.Select(s => s.Student?.ToBaseDto()).ToList();
            elective.Groups = groups ?? new List<GroupDto>();
            elective.Students = students ?? new List<StudentDto>();
            return elective;
        }

        public static Elective ToDbo(this ElectiveDto e)
        {
            if (e == null)
                return null;
            var elective = e.ToBaseDbo();
            List<ElectiveGroup> groups = e.Groups?.Select(g => new ElectiveGroup
                                                              {
                                                                  ElectiveId = e.Id,
                                                                  GroupId = g.Id
                                                              }).ToList();
            List<StudentElective> students = e.Students?.Select(s => new StudentElective
                                                              {
                                                                  ElectiveId = e.Id,
                                                                  StudentId = s.Id
                                                              }).ToList();
            elective.Groups = groups;
            elective.Students = students;
            return elective;
        }
    }
}
