﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class StudentConverter
    {
        public static StudentDto ToBaseDto(this Student s)
        {
            if (s == null)
                return null;
            StudentDto student = s.ToDto<StudentDto, Student>();
            student.Group = s.Group?.ToBaseDto();
            student.UserId = s.UserId;
            return student;
        }

        public static Student ToBaseDbo(this StudentDto s)
        {
            if (s == null)
                return null;
            var student = s.ToDbo<StudentDto, Student>();
            student.GroupId = s.Group?.Id ?? 0;
            student.UserId = s.UserId;
            return student;
        }

        public static StudentDto ToDto(this Student s)
        {
            if (s == null)
                return null;
            var student = s.ToBaseDto();
            List<ElectiveDto> electives = s.Electives?.Where(e => e.Approved).Select(e => e.Elective.ToBaseDto()).ToList();
            List<ElectiveDto> pending = s.Electives?.Where(e => !e.Approved).Select(e => e.Elective.ToBaseDto()).ToList();
            student.Electives = electives ?? new List<ElectiveDto>();
            student.PendingElectives = pending ?? new List<ElectiveDto>();
            return student;
        }

        public static Student ToDbo(this StudentDto s)
        {
            if (s == null)
                return null;
            var student = s.ToBaseDbo();

            var approved = s.Electives?.Select(e => new StudentElective
                                                    {
                                                        ElectiveId = e.Id,
                                                        StudentId = s.Id,
                                                        Approved = true
                                                    })
                        ?? new List<StudentElective>();
            var pending = s.PendingElectives?.Select(e => new StudentElective
                                                          {
                                                              ElectiveId = e.Id,
                                                              StudentId = s.Id,
                                                              Approved = false
                                                          })
                       ?? new List<StudentElective>();

            List<StudentElective> electives = approved.Union(pending).ToList();
            student.Electives = electives;
            return student;
        }
    }
}
