﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class GroupConverter
    {
        public static GroupDto ToBaseDto(this Group g)
        {
            if (g == null)
                return null;
            return new GroupDto
                   {
                        Id = g.Id,
                        Name = g.Name
                   };
        }

        public static Group ToBaseDbo(this GroupDto g)
        {
            if (g == null)
                return null;
            return new Group
                   {
                       Id = g.Id,
                       Name = g.Name
                   };
        }

        public static GroupDto ToDto(this Group g)
        {
            if (g == null)
                return null;
            var group = g.ToBaseDto();
            List<ElectiveDto> electives = g.Electives?.Select(e => e.Elective?.ToBaseDto()).ToList();
            List<StudentDto> students = g.Students?.Select(s => s?.ToBaseDto()).ToList();
            group.Electives = electives ?? new List<ElectiveDto>();
            group.Students = students ?? new List<StudentDto>();
            return group;
        }

        public static Group ToDbo(this GroupDto g)
        {
            if (g == null)
                return null;
            var group = g.ToBaseDbo();
            List<ElectiveGroup> electives = g.Electives?.Select(e => new ElectiveGroup
                                                              {
                                                                  ElectiveId = e.Id,
                                                                  GroupId = g.Id
                                                              }).ToList();
            List<Student> students = g.Students?.Select(s => s.ToBaseDbo()).ToList();
            group.Electives = electives;
            group.Students = students;
            return group;
        }
    }
}
