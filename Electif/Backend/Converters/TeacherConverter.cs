﻿using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class TeacherConverter
    {

        public static TeacherDto ToBaseDto(this Teacher t)
        {
            TeacherDto teacher = t?.ToDto<TeacherDto, Teacher>();
            return teacher;
        }
        public static TeacherDto ToDto(this Teacher t)
        {
            if (t == null)
                return null;
            TeacherDto teacher = t.ToDto<TeacherDto, Teacher>();
            List<ElectiveDto> electivesList = t.Electives?.Select(e => e.ToBaseDto()).ToList();
            teacher.Electives = electivesList ?? new List<ElectiveDto>();
            return teacher;
        }

        public static Teacher ToDbo(this TeacherDto t)
        {
            if (t == null)
                return null;
            Teacher teacher = t.ToDbo<TeacherDto, Teacher>();
            List<Elective> electivesList = t.Electives?.Select(e => e.ToBaseDbo()).ToList();
            teacher.Electives = electivesList;
            return teacher;
        }
    }
}
