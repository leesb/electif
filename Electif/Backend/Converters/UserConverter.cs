﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class UserConverter
    {
        public static TDto ToDto<TDto, TDal>(this TDal u)
            where TDto : UserDto, new()
            where TDal : User
        {
            if (u == null)
                return null;
            var user = new TDto
                        {
                            Id = u.Id,
                            Firstname = u.Firstname,
                            Lastname = u.Lastname,
                            Email = u.Email
                        };
            return user;
        }

        public static TDal ToDbo<TDto, TDal>(this TDto u)
            where TDto : UserDto
            where TDal : User, new()
        {
            if (u == null)
                return null;
            var user = new TDal
                        {
                            Id = u.Id,
                            Firstname = u.Firstname,
                            Lastname = u.Lastname,
                            Email = u.Email,
                            Password = u.Password,
                            Salt = u.Salt
                        };
            return user;
        }
    }
}
