﻿using Backend.Models;
using Service.Models;

namespace Backend.Converters
{
    public static class AdminConverter
    {
        public static AdminDto ToDto(this Admin a)
        {
            AdminDto admin = a.ToDto<AdminDto, Admin>();
            return admin;
        }

        public static Admin ToDbo(this AdminDto a)
        {
            Admin admin = a.ToDbo<AdminDto, Admin>();
            return admin;
        }
    }
}
