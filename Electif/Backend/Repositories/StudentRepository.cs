﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Helpers;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class StudentRepository : IStudentService
    {
        private readonly ElectifContext context;
        private readonly IElectiveService electiveService;


        public StudentRepository(ElectifContext context, IElectiveService electiveService)
        {
            this.context = context;
            this.electiveService = electiveService;
        }

        private IQueryable<Student> BaseRequest()
        {
            return context.Students
                          .Include(s => s.Electives).ThenInclude(se => se.Elective).ThenInclude(e => e.Teacher)
                          .Include(s => s.Group);
        }

        public async Task<StudentDto> GetById(int id)
        {
            var student = await BaseRequest().FirstOrDefaultAsync(s => s.Id == id);
            if (student == null)
            {
                return new StudentDto
                       {
                           ErrorMessage = "Student not found"
                       };
            }
            return student.ToDto();
        }

        public async Task<List<StudentDto>> Get()
        {
            var students = await BaseRequest().Select(s => s.ToDto()).ToListAsync();
            return students;
        }

        public async Task<StudentDto> Add(StudentDto dto)
        {
            if (dto == null)
            {
                return new StudentDto
                       {
                           ErrorMessage = "Can't add null student"
                       };
            }

            var salt = SecurityHelper.GenerateSalt();
            var hashed = SecurityHelper.HashSHA1(salt, dto.Password);
            var stringSalt = Convert.ToBase64String(salt);
            dto.Salt = stringSalt;
            dto.Password = hashed;

            var inserted = context.Students.Add(dto.ToDbo());
            await context.SaveChangesAsync();
            return inserted.Entity.ToDto();
        }

        public async Task<StudentDto> Update(StudentDto dto)
        {
            if (dto == null)
            {
                return new StudentDto
                       {
                           ErrorMessage = "Can't update null student"
                       };
            }

            var dbo = await BaseRequest().FirstOrDefaultAsync(s => s.Id == dto.Id);
            if (dbo == null)
            {
                return new StudentDto
                       {
                           ErrorMessage = $"Can't update #{dto.Id} student"
                       };
            }
            var converted = dto.ToDbo();
            dbo.Email = converted.Email;
            dbo.Firstname = converted.Firstname;
            dbo.Lastname = converted.Lastname;
            dbo.Password = converted.Password;
            dbo.Salt = converted.Salt;
            dbo.Electives = converted.Electives;
            dbo.GroupId = converted.GroupId;
            dbo.UserId = converted.UserId;
            dbo.Id = converted.Id;
            await context.SaveChangesAsync();
            var result = dbo.ToDto();
            return result;
        }

        public async Task<StudentDto> Delete(int id)
        {
            var toRemove = await BaseRequest().SingleOrDefaultAsync(s => s.Id == id);
            if (toRemove == null)
            {
                return new StudentDto
                       {
                           ErrorMessage = $"Can't delete #{id}"
                       };
            }

            var student = toRemove.ToDto();
            context.Students.Remove(toRemove);
            await context.SaveChangesAsync();
            return student;
        }

        public async Task<StudentDto> Subscribe(int id, int electiveId)
        {
            var student = await BaseRequest().FirstOrDefaultAsync(s => s.Id == id);
            if (student == null)
                return new StudentDto { ErrorMessage = $"Student with id {id} does not exist"};
            var elective = await electiveService.GetById(electiveId);
            if (!string.IsNullOrEmpty(elective.ErrorMessage))
                return new StudentDto {ErrorMessage = $"Elective with id {electiveId} does not exist"};

            var se = student.Electives.FirstOrDefault(s => s.ElectiveId == electiveId);
            if (se != null)
            {
                return student.ToDto();
            }

            var group = elective.Groups.FirstOrDefault(g => g.Id == student.GroupId);
            if (group == null)
            {
                return new StudentDto{ ErrorMessage = $"Elective {electiveId} does not accept group {student.GroupId}"};
            }

            student.Electives.Add(new StudentElective
                                  {
                                      Approved = false,
                                      StudentId = student.Id,
                                      ElectiveId = electiveId
                                  });

            await context.SaveChangesAsync();

            return student.ToDto();
        }

        public async Task<List<StudentDto>> GetByElective(int electiveId)
        {
            var students = await BaseRequest().Where(s => s.Electives.Any(e => e.ElectiveId == electiveId))
                                              .Select(s => s.ToDto())
                                              .ToListAsync();

            return students;
        }
    }
}
