﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Helpers;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class TeacherRepository : ITeacherService
    {
        private ElectifContext context;

        public TeacherRepository(ElectifContext context)
        {
            this.context = context;
        }


        private IQueryable<Teacher> BaseRequest()
        {
            return context.Teachers.Include(t => t.Electives);
        }

        public async Task<TeacherDto> GetById(int id)
        {
            var teacher = await BaseRequest().FirstOrDefaultAsync(a => a.Id == id);
            if (teacher == null)
            {
                return new TeacherDto
                       {
                           ErrorMessage = "Teacher not found"
                       };
            }
            return teacher.ToDto();
        }

        public async Task<List<TeacherDto>> Get()
        {
            var teachers = await BaseRequest().Select(t => t.ToDto()).ToListAsync();
            return teachers;
        }

        public async Task<TeacherDto> Add(TeacherDto dto)
        {
            if (dto == null)
            {
                return new TeacherDto
                       {
                           ErrorMessage = "Can't add null teacher"
                       };
            }

            var salt = SecurityHelper.GenerateSalt();
            var hashed = SecurityHelper.HashSHA1(salt, dto.Password);
            var stringSalt = Convert.ToBase64String(salt);
            dto.Salt = stringSalt;
            dto.Password = hashed;

            var inserted = context.Teachers.Add(dto.ToDbo());
            await context.SaveChangesAsync();
            return inserted.Entity.ToDto();
        }

        public async Task<TeacherDto> Update(TeacherDto dto)
        {
            if (dto == null)
            {
                return new TeacherDto
                       {
                           ErrorMessage = "Can't update null teacher"
                       };
            }

            var dbo = await BaseRequest().FirstOrDefaultAsync(t => t.Id == dto.Id);
            if (dbo == null)
            {
                return new TeacherDto
                       {
                           ErrorMessage = $"Can't update #{dto.Id} teacher"
                       };
            }
            var converted = dto.ToDbo();
            dbo.Electives = converted.Electives;
            dbo.Email = converted.Email;
            dbo.Firstname = converted.Firstname;
            dbo.Lastname = converted.Lastname;
            dbo.Password = converted.Password;
            dbo.Salt = converted.Salt;
            dbo.Id = converted.Id;
            await context.SaveChangesAsync();
            var result = dbo.ToDto();
            return result;
        }

        public async Task<TeacherDto> Delete(int id)
        {
            var toRemove = await BaseRequest().SingleOrDefaultAsync(e => e.Id == id);
            if (toRemove == null)
            {
                return new TeacherDto
                       {
                           ErrorMessage = $"Can't delete #{id}"
                       };
            }

            var teacher = toRemove.ToDto();
            context.Teachers.Remove(toRemove);
            await context.SaveChangesAsync();
            return teacher;
        }
    }
}
