﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class GroupRepository : IGroupService
    {
        private readonly ElectifContext context;

        public GroupRepository(ElectifContext context)
        {
            this.context = context;
        }

        private IQueryable<Group> BaseRequest()
        {
            return context.Groups
                          .Include(g => g.Electives)
                          .ThenInclude(eg => eg.Elective)
                          .Include(g => g.Students);
        }


        public async Task<GroupDto> GetById(int id)
        {
            var group = await BaseRequest().FirstOrDefaultAsync(g => g.Id == id);
            if (group == null)
            {
                return new GroupDto
                       {
                           ErrorMessage = "Group not found"
                       };
            }
            return group.ToDto();
        }

        public async Task<List<GroupDto>> Get()
        {
            var groups = await BaseRequest().Select(g => g.ToDto()).ToListAsync();
            return groups;
        }

        public async Task<GroupDto> Add(GroupDto dto)
        {
            if (dto == null)
            {
                return new GroupDto
                       {
                           ErrorMessage = "Can't add null group"
                       };
            }
            var inserted = context.Groups.Add(dto.ToDbo());
            await context.SaveChangesAsync();
            return inserted.Entity.ToDto();
        }

        public async Task<GroupDto> Update(GroupDto dto)
        {
            if (dto == null)
            {
                return new GroupDto
                       {
                           ErrorMessage = "Can't update null group"
                       };
            }
            var dbo = await BaseRequest().FirstOrDefaultAsync(g => g.Id == dto.Id);
            if (dbo == null)
            {
                return new GroupDto
                       {
                           ErrorMessage = $"Can't update #{dto.Id} group"
                       };
            }
            var converted = dto.ToDbo();
            dbo.Electives = converted.Electives;
            dbo.Name = converted.Name;
            dbo.Students = converted.Students;
            dbo.Id = converted.Id;
            await context.SaveChangesAsync();
            var result = dbo.ToDto();
            return result;
        }

        public async Task<GroupDto> Delete(int id)
        {
            var toRemove = await BaseRequest().SingleOrDefaultAsync(g => g.Id == id);
            if (toRemove == null)
            {
                return new GroupDto
                       {
                           ErrorMessage = $"Can't delete #{id}"
                       };
            }

            var group = toRemove.ToDto();
            context.Groups.Remove(toRemove);
            await context.SaveChangesAsync();
            return group;
        }
    }
}
