﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Helpers;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class AdminRepository : IAdminService
    {
        private readonly ElectifContext context;
        private readonly IStudentService studentService;

        public AdminRepository(ElectifContext context, IStudentService studentService)
        {
            this.context = context;
            this.studentService = studentService;
        }

        public async Task<AdminDto> GetById(int id)
        {
            var admin = await context.Admins.FirstOrDefaultAsync(a => a.Id == id);
            if (admin == null)
            {
                return new AdminDto
                       {
                           ErrorMessage = "Admin not found"
                       };
            }
            return admin.ToDto();
        }

        public async Task<List<AdminDto>> Get()
        {
            var admins = await context.Admins.Select(a => a.ToDto()).ToListAsync();
            return admins;
        }

        public async Task<AdminDto> Add(AdminDto dto)
        {
            if (dto == null)
            {
                return new AdminDto
                       {
                           ErrorMessage = "Can't add null admin"
                       };
            }

            var salt = SecurityHelper.GenerateSalt();
            var hashed = SecurityHelper.HashSHA1(salt, dto.Password);
            var stringSalt = Convert.ToBase64String(salt);
            dto.Salt = stringSalt;
            dto.Password = hashed;

            var inserted = context.Admins.Add(dto.ToDbo());
            await context.SaveChangesAsync();
            return inserted.Entity.ToDto();
        }

        public async Task<AdminDto> Update(AdminDto dto)
        {
            if (dto == null)
            {
                return new AdminDto
                       {
                           ErrorMessage = "Can't update null admin"
                       };
            }

            var dbo = await context.Admins.FirstOrDefaultAsync(a => a.Id == dto.Id);
            if (dbo == null)
            {
                return new AdminDto
                       {
                           ErrorMessage = $"Can't update #{dto.Id} admin"
                       };
            }
            var converted = dto.ToDbo();
            dbo.Email = converted.Email;
            dbo.Firstname = converted.Firstname;
            dbo.Lastname = converted.Lastname;
            dbo.Password = converted.Password;
            dbo.Salt = converted.Salt;
            dbo.Id = converted.Id;
            await context.SaveChangesAsync();
            var result = dbo.ToDto();
            return result;
        }

        public async Task<AdminDto> Delete(int id)
        {
            var toRemove = await context.Admins.SingleOrDefaultAsync(a => a.Id == id);
            if (toRemove == null)
            {
                return new AdminDto
                       {
                           ErrorMessage = $"Can't delete #{id}"
                       };
            }

            var admin = toRemove.ToDto();
            context.Admins.Remove(toRemove);
            await context.SaveChangesAsync();
            return admin;
        }

        public async Task<bool> ValidateStudentElective(int studentId, int electiveId)
        {
            var student = await studentService.GetById(studentId);
            if (!string.IsNullOrEmpty(student.ErrorMessage))
                return false;

            if (student.PendingElectives.All(e => e.Id != electiveId))
            {
                return false;
            }

            var se = await context.StudentElectives
                                  .FirstOrDefaultAsync(s => s.StudentId == studentId && s.ElectiveId == electiveId);

            se.Approved = true;

            await context.SaveChangesAsync();

            return true;
        }
    }
}
