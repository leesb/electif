﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class UserRepository : IUserService
    {
        private readonly ElectifContext context;
        private readonly JwtConfiguration jwt;

        public UserRepository(ElectifContext context, JwtConfiguration jwt)
        {
            this.context = context;
            this.jwt = jwt;
        }

        public async Task<UserDto> GetById(int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);
            switch (user)
            {
                case null:
                    return new UserDto {ErrorMessage = $"User {id} does not exist"};
                case Student student:
                    return student.ToDto();
                case Admin admin:
                    return admin.ToDto();
                case Teacher teacher:
                    return teacher.ToDto();
                default:
                    return user.ToDto<UserDto, User>();
            }
        }

        public async Task<string> GetRole(int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);
            switch (user)
            {
                case null:
                    return null;
                case Student student:
                    return "Student";
                case Admin admin:
                    return "Admin";
                case Teacher teacher:
                    return "Teacher";
                default:
                    return null;
            }
        }

        public async Task<JsonWebToken> Login(LoginModel model)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Email.Equals(model.Email));
            if (user == null)
                return null;
            var salt = Convert.FromBase64String(user.Salt);
            var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(password: model.Password,
                                                                     salt: salt,
                                                                     prf: KeyDerivationPrf.HMACSHA1,
                                                                     iterationCount: 10000,
                                                                     numBytesRequested: 256 / 8));
            if (!hashed.Equals(user.Password))
                return null;

            var claims = new[]
                         {
                             new Claim(JwtRegisteredClaimNames.Sub, model.Email),
                             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                         };

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwt.SecretKey));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiredOn = DateTime.Now.AddMinutes(jwt.TokenExpirationTime);
            var token = new JwtSecurityToken(jwt.ValidIssuer,
                                             jwt.ValidAudience,
                                             claims,
                                             expires: expiredOn,
                                             signingCredentials: cred);

            var role = "user";
            switch (user)
            {
                case Admin _:
                    role = "Admin";
                    break;
                case Student _:
                    role = "Student";
                    break;
                case Teacher _:
                    role = "Teacher";
                    break;
            }

            var access = new JsonWebToken
                         {
                             ExpireOnDate = token.ValidTo,
                             Success = true,
                             ExpiryIn = jwt.TokenExpirationTime,
                             Token = new JwtSecurityTokenHandler().WriteToken(token),
                             Id = user.Id,
                             Email = user.Email,
                             Firstname = user.Firstname,
                             Lastname = user.Lastname,
                             Role = role
                         };
            return access;
        }
    }
}
