﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Converters;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;
using Service.Models;

namespace Backend.Repositories
{
    public class ElectiveRepository : IElectiveService
    {
        private readonly ElectifContext context;

        public ElectiveRepository(ElectifContext context)
        {
            this.context = context;
        }

        private IQueryable<Elective> BaseRequest()
        {
            return context.Electives
                          .Include(e => e.Teacher)
                          .Include(e => e.Groups).ThenInclude(eg => eg.Group)
                          .Include(e => e.Students).ThenInclude(se => se.Student);
        }

        public async Task<ElectiveDto> GetById(int id)
        {
            var elective = await BaseRequest().FirstOrDefaultAsync(a => a.Id == id);
            if (elective == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = "Elective not found"
                       };
            }
            return elective.ToDto();
        }

        public async Task<List<ElectiveDto>> Get()
        {
            var electives = await BaseRequest().Select(t => t.ToDto()).ToListAsync();
            return electives;
        }

        public async Task<ElectiveDto> Add(ElectiveDto dto)
        {
            if (dto == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = "Can't add null elective"
                       };
            }

            var inserted = context.Electives.Add(dto.ToDbo());
            await context.SaveChangesAsync();
            return inserted.Entity.ToDto();
        }

        public async Task<ElectiveDto> Update(ElectiveDto dto)
        {
            if (dto == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = "Can't update null elective"
                       };
            }

            var dbo = await BaseRequest().FirstOrDefaultAsync(t => t.Id == dto.Id);
            if (dbo == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = $"Can't update #{dto.Id} elective"
                       };
            }
            var converted = dto.ToDbo();
            dbo.Acronym = converted.Acronym;
            dbo.Description = converted.Description;
            dbo.Groups = converted.Groups;
            dbo.Students = converted.Students;
            dbo.Name = converted.Name;
            dbo.TeacherId = converted.TeacherId;
            dbo.Id = converted.Id;
            await context.SaveChangesAsync();
            var result = dbo.ToDto();
            return result;
        }

        public async Task<ElectiveDto> Delete(int id)
        {
            var toRemove = await BaseRequest().SingleOrDefaultAsync(e => e.Id == id);
            if (toRemove == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = $"Can't delete #{id}"
                       };
            }

            var elective = toRemove.ToDto();
            context.Electives.Remove(toRemove);
            await context.SaveChangesAsync();
            return elective;
        }

        public async Task<List<ElectiveDto>> GetByGroup(int groupId)
        {
            var electives = await BaseRequest().Where(e => e.Groups.Any(eg => eg.GroupId == groupId))
                                               .Select(e => e.ToDto())
                                               .ToListAsync();
            return electives;
        }

        public async Task<List<ElectiveDto>> GetByTeacher(int teacherId)
        {
            var electives = await BaseRequest().Where(e => e.TeacherId == teacherId)
                                               .Select(e => e.ToDto())
                                               .ToListAsync();
            return electives;
        }

        public async Task<ElectiveDto> RemoveStudent(int electiveId, int studentId)
        {
            var dbo = await BaseRequest().FirstOrDefaultAsync(t => t.Id == electiveId);
            if (dbo == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = $"Elective #{electiveId} does not exist"
                       };
            }
            var es = context.StudentElectives.FirstOrDefault(s => s.ElectiveId == electiveId && s.StudentId == studentId);
            if (es == null)
                return dbo.ToDto();
            dbo.Students.Remove(es);
            await context.SaveChangesAsync();
            return dbo.ToDto();
        }

        public async Task<ElectiveDto> ApproveStudent(int electiveId, int studentId)
        {
            var es = context.StudentElectives.FirstOrDefault(s => s.ElectiveId == electiveId && s.StudentId == studentId);
            if (es == null)
                return new ElectiveDto{ ErrorMessage = $"No relation exist between elective {electiveId} and student {studentId}" };
            es.Approved = true;
            await context.SaveChangesAsync();
            return await GetById(electiveId);
        }

        public async Task<ElectiveDto> AddGroup(int electiveId, int groupId)
        {
            var elective = await GetById(electiveId);
            if (!string.IsNullOrEmpty(elective.ErrorMessage))
                return elective;
            var group = context.Groups.FirstOrDefault(g => g.Id == groupId);
            if (group == null)
                return new ElectiveDto{ ErrorMessage = $"Group {groupId} does not exist"};
            context.ElectiveGroups.Add(new ElectiveGroup
                                       {
                                           ElectiveId = electiveId,
                                           GroupId = groupId
                                       });
            await context.SaveChangesAsync();
            return await GetById(electiveId);
        }

        public async Task<ElectiveDto> RemoveGroup(int electiveId, int groupId)
        {
            var dbo = await BaseRequest().FirstOrDefaultAsync(t => t.Id == electiveId);
            if (dbo == null)
            {
                return new ElectiveDto
                       {
                           ErrorMessage = $"Elective #{electiveId} does not exist"
                       };
            }
            var es = context.ElectiveGroups.FirstOrDefault(s => s.ElectiveId == electiveId && s.GroupId == groupId);
            if (es == null)
                return dbo.ToDto();
            dbo.Groups.Remove(es);
            await context.SaveChangesAsync();
            return dbo.ToDto();
        }
    }
}
