﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Repositories
{
    public class ElectifContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentElective> StudentElectives { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Elective> Electives { get; set; }
        public DbSet<ElectiveGroup> ElectiveGroups { get; set; }
        public DbSet<Group> Groups { get; set; }

        public ElectifContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(u => u.Email).HasName("AK_User_Email");

            modelBuilder.Entity<Admin>().HasBaseType<User>();

            modelBuilder.Entity<Student>().HasBaseType<User>();

            modelBuilder.Entity<Student>().HasIndex(s => s.UserId).IsUnique();

            modelBuilder.Entity<Teacher>().HasBaseType<User>();

            modelBuilder.Entity<Group>().HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<Elective>().HasIndex(e => e.Acronym).IsUnique();

            modelBuilder.Entity<ElectiveGroup>().HasKey(eg => new {eg.ElectiveId, eg.GroupId});

            modelBuilder.Entity<ElectiveGroup>()
                        .HasOne(eg => eg.Elective)
                        .WithMany(e => e.Groups)
                        .HasForeignKey(eg => eg.ElectiveId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ElectiveGroup>()
                        .HasOne(eg => eg.Group)
                        .WithMany(g => g.Electives)
                        .HasForeignKey(eg => eg.GroupId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentElective>().HasKey(se => new {se.StudentId, se.ElectiveId});

            modelBuilder.Entity<StudentElective>()
                        .HasOne(se => se.Student)
                        .WithMany(s => s.Electives)
                        .HasForeignKey(se => se.StudentId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentElective>()
                        .HasOne(se => se.Elective)
                        .WithMany(e => e.Students)
                        .HasForeignKey(se => se.ElectiveId)
                        .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
