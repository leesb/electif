using System;
using System.Text;
using Backend.Models;
using Backend.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Service.Interfaces;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING")
                                   ?? Configuration.GetConnectionString("Electif");
            services.AddDbContext<ElectifContext>(o => o.UseSqlServer(connectionString));
            services.AddScoped<IStudentService, StudentRepository>();
            services.AddScoped<ITeacherService, TeacherRepository>();
            services.AddScoped<IAdminService, AdminRepository>();
            services.AddScoped<IElectiveService, ElectiveRepository>();
            services.AddScoped<IGroupService, GroupRepository>();
            services.AddScoped<IUserService, UserRepository>();
            var jwt = new JwtConfiguration
                      {
                          SecretKey = "7094f07f-c661-43dc-8ee6-de8378531b7a",
                          TokenExpirationTime = 60 * 24,
                          ValidAudience = "c5663d74-b831-4bec-a359-ad92887e247e",
                          ValidIssuer = "e98688e1-9272-44d8-b33a-ddd09d65731a"
                      };
            services.AddSingleton(jwt);
            services.AddControllers()
                .AddNewtonsoftJson();
            var tokenValidationParameters = new TokenValidationParameters
                                            {
                                                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwt.SecretKey)),
                                                ValidIssuer = jwt.ValidIssuer,
                                                ValidAudience = jwt.ValidAudience
                                            };
            services.AddAuthentication(o =>
                                       {
                                           o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                                           o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                                       })
                    .AddJwtBearer(c =>
                                  {
                                      c.RequireHttpsMetadata = false;
                                      c.SaveToken = true;
                                      c.TokenValidationParameters = tokenValidationParameters;
                                  });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }
            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
